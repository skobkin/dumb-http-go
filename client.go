package simple_http

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
)

type Client struct {
	HttpClient http.Client
}

func (c *Client) getResponseBody(resp *http.Response) ([]byte, error) {
	body, readErr := ioutil.ReadAll(resp.Body)

	if readErr != nil {
		log.Fatal(readErr)
		return nil, readErr
	}

	return body, nil
}

func (c *Client) MakeGetRequest(url string, params *url.Values, headers *map[string]string) ([]byte, error) {
	var body []byte

	if params != nil {
		url += "?" + params.Encode()
	}

	req, reqErr := http.NewRequest("GET", url, nil)

	if reqErr != nil {
		log.Fatal(reqErr)
		return body, reqErr
	}

	// Headers
	if headers != nil {
		for key, value := range *headers {
			req.Header.Set(key, value)
		}
	}

	resp, respErr := c.HttpClient.Do(req)

	if respErr != nil {
		log.Fatal(respErr)
		return nil, respErr
	}

	defer resp.Body.Close()

	body, readErr := c.getResponseBody(resp)

	if readErr != nil {
		return nil, readErr
	}

	return body, nil
}

func (c *Client) MakePostRequest(url string, params url.Values, headers *map[string]string) ([]byte, error) {
	req, reqErr := http.NewRequest("POST", url, strings.NewReader(params.Encode()))

	if reqErr != nil {
		log.Fatal(reqErr)
		return nil, reqErr
	}

	// Adding necessary for POST request header
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	// Headers
	if headers != nil {
		for key, value := range *headers {
			req.Header.Set(key, value)
		}
	}

	resp, respErr := c.HttpClient.Do(req)

	if respErr != nil {
		log.Fatal(respErr)
		return nil, respErr
	}

	body, readErr := c.getResponseBody(resp)

	if readErr != nil {
		return nil, readErr
	}

	return body, nil
}
